import Vue from "vue";
import App from "./App.vue";
import router from "@/router";
import createStore from "./store/index";
import '@/assets/css/tailwind.css'
Vue.config.productionTip = false;

router.beforeEach((to, from, next) => {
  if (to.name !== "login" && store.state.authenticated == false)
    next({ name: "login" });
  next();
});
const store = createStore();
new Vue({
  router,
  render: (h) => h(App),
  store: store,
}).$mount("#app");
