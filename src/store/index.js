import Vue from "vue";
import Vuex from "vuex";
import initialState from "./initialState";
import mutations from "./mutations";
import actions from "./actions";
import getters from "./getters";
Vue.use(Vuex);

const state = () => initialState();

const createStore = () =>
  new Vuex.Store({
    state,
    mutations,
    actions,
    getters,
  });

export default createStore;
