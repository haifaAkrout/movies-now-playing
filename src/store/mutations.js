import {
  SET_AUTHENTICATED,
  SET_MOVIES_NOW_PLAYING,
  SET_IMAGES,
  SET_MOVIES,
  SET_POSTER_SIZE,
  SET_RATE,
  SET_GENRES,
  SET_CHECKED,
  SET_MOVIES_CHECKED,
} from "./mutation-types";

export default {
  [SET_AUTHENTICATED](state, payload) {
    state.authenticated = payload;
  },

  [SET_MOVIES_NOW_PLAYING](state, movies) {
    state.filteredMovies = movies;
  },
  [SET_MOVIES](state, movies) {
    state.movies = movies;
  },
  [SET_MOVIES_CHECKED](state, movies) {
    state.checked_movies = movies;
  },
  [SET_IMAGES](state, images) {
    state.images = images;
  },
  [SET_POSTER_SIZE](state, size) {
    state.posterSize = size;
  },
  [SET_RATE](state, rate) {
    state.rate = rate;
    if (state.checked === undefined || state.checked.length == 0)
      state.filteredMovies = state.movies;
    else state.filteredMovies = state.checked_movies;
    state.filteredMovies = state.filteredMovies.filter((item) => {
      return item.vote_average >= state.rate;
    });
  },
  [SET_GENRES](state, genres) {
    state.genres = genres;
    console.log(state.genres);
  },
  [SET_CHECKED](state, checked) {
    var exist = false;
    state.filteredMovies = state.movies;
    state.checked = checked;
    state.filteredMovies = state.filteredMovies.filter((item) => {
      var genres_names = item.genres.map(function(val) {
        return val.name;
      });
      exist = state.checked.every(function(val) {
        return genres_names.indexOf(val.name) >= 0;
      });
      if (exist == true) {
        return item;
      }
    });
    state.checked_movies = state.filteredMovies;
  },
};
