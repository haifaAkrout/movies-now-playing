import axios from "axios";

const getMovies = ({ commit }) => {
  axios
    .get(
      "https://api.themoviedb.org/3/movie/now_playing?api_key=8c06a89ce1dfc3e927078f13a6994e93&language=en-US&page=1"
    )
    .then(
      (response) => {
        response.data.results.forEach(function(item) {
          axios

            .get(
              "https://api.themoviedb.org/3/movie/" +
                item.id +
                "?api_key=8c06a89ce1dfc3e927078f13a6994e93&language=en-US"
            )
            .then((response1) => {
              item.genres = response1.data.genres;
            });
        });

        var movies = [];
        movies = response.data.results.sort((a, b) => {
          return b.popularity - a.popularity;
        });

        commit("SET_MOVIES_NOW_PLAYING", movies);
        commit("SET_MOVIES", movies);
      },
      (err) => {
        console.log(err);
      }
    );
};
const getImages = ({ commit }) => {
  axios
    .get(
      "https://api.themoviedb.org/3/configuration?api_key=8c06a89ce1dfc3e927078f13a6994e93"
    )
    .then(
      (response1) => {
        commit("SET_IMAGES", response1.data.images);
        commit("SET_POSTER_SIZE", response1.data.images.poster_sizes[3]);
      },
      (err) => {
        console.log(err);
      }
    );
};
const getGenres = ({ commit }) => {
  axios
    .get(
      "https://api.themoviedb.org/3/genre/movie/list?api_key=8c06a89ce1dfc3e927078f13a6994e93&language=en-US"
    )
    .then(
      (response) => {
        commit("SET_GENRES", response.data.genres);
      },
      (err) => {
        console.log(err);
      }
    );
};

export default {
  getMovies,
  getImages,
  getGenres
};
