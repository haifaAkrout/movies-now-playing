export default () => ({
  authenticated: false,
  movies: [],
  filteredMovies: [],
  images: [],
  posterSize: "",
  genres:[],
  rate: "",
  checked:[],
  checked_movies :[]
});
