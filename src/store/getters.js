const filteredMovies = (state) => state.filteredMovies;
const images = (state) => state.images;
const genres=(state)=>state.genres;
const posterSize = (state) => state.posterSize;
const rate = (state) => state.rate;
const checked=(state)=>state.checked;

export default {
  filteredMovies,
  images,
  posterSize,
  rate,
  genres,
  checked
};
