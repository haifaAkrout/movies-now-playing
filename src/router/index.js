import Vue from "vue";
import VueRouter from "vue-router";
import LoginComponent from "../components/Login.vue";
import ShowMoviesComponent from "../components/ShowMovies.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/login",
      name: "login",
      component: LoginComponent,
    },
    {
      path: "/showmovies",
      name: "showmovies",
      component: ShowMoviesComponent,
    },
  ],
});
export default router;
